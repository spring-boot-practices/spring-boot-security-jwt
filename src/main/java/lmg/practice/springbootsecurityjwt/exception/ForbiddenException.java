package lmg.practice.springbootsecurityjwt.exception;

public class ForbiddenException extends RuntimeException{

    public ForbiddenException(Throwable cause) {
        super(cause);
    }
}
