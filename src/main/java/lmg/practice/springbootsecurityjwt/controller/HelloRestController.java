package lmg.practice.springbootsecurityjwt.controller;

import lmg.practice.springbootsecurityjwt.exception.ForbiddenException;
import lmg.practice.springbootsecurityjwt.model.AuthenticationRequest;
import lmg.practice.springbootsecurityjwt.model.AuthenticationResponse;
import lmg.practice.springbootsecurityjwt.service.MyUserDetailsService;
import lmg.practice.springbootsecurityjwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("hello")
public class HelloRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @PostMapping("authentication")
    public ResponseEntity<AuthenticationResponse> getAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
                    authenticationRequest.getPassword()));
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(token));
    }

    @GetMapping("user")
    public String helloUser() {
        return "Hello User";
    }

    @GetMapping("admin")
    public String helloAdmin() {
        return "Hello Admin";
    }
}